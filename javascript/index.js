var $arr = Array();

var $arr2 = [];

var $arr3 = [
	"1","senin","2","selasa",
]; 

var $arr4 = {
	"response_code":"00",
	"response_message":"Data berhasil disimpan",
	"data":{
		'nama':'Bagudung',
		'email':'gudung@tikustanah.co.id'
	}	
}

console.log('isi array adalah : ' + $arr3.length);
console.log('tipe array adalah : ' + typeof($arr3));

for(let i = 0; i < $arr3.length; i++){

  console.log($arr3[i]);
  
}

console.log($arr4.response_code);
console.log($arr4.response_message);
console.log($arr4.data);
console.log($arr4.data.nama);
console.log($arr4.data.email);

var daftarBuah = ["20. Apel", "5. Jeruk", "23. Anggur", "45. Semangka", "1. Mangga"];

var daftarBuah2 = ["20. Apel", "5. Jeruk", "23. Anggur", "45. Semangka", "1. Mangga"];

var defaultSortedDaftarBuah = daftarBuah.sort(); // tidak cukup jika hanya menggunakan sort standard, karena bisa terjadi kesalahan

var sortedDaftarBuah = daftarBuah2.sort(
  (a,b)=>{

    let numA = parseInt(a.split('.')[0]); 
    let numB = parseInt(b.split('.')[0]);

    if(numA > numB) return 1;
    if(numA < numB) return -1;
    if(numA == numB) return 0;

  }
); 

console.log(sortedDaftarBuah);

jwb1 = document.getElementById("jawabanSoal1");
jwb2 = document.getElementById("jawabanSoal2");
jwb3 = document.getElementById("jawabanSoal3");

txt1 = "";
for(let i = 0; i < sortedDaftarBuah.length; i++){

	console.log(sortedDaftarBuah[i]);
  txt1 += sortedDaftarBuah[i] + "<br>"

}

jwb1.innerHTML = txt1; 

var kalimat = "saya sangat senang belajar javascript"

kata_kata=kalimat.split(' ');

txt2 = kata_kata;

console.log(txt2);

jwb2.innerHTML = JSON.stringify(txt2);

let data = 
"1.nama: strawberry\n" +
  "warna: merah\n" +
  "ada bijinya: tidak\n" +
  "harga: 9000\n" +
"2.nama: jeruk\n" +
  "warna: oranye\n" +
  "ada bijinya: ada\n" +
  "harga: 8000\n" +
"3.nama: Semangka\n" +
  "warna: Hijau & Merah\n" +
  "ada bijinya: ada\n" +
  "harga: 10000\n" +
"4.nama: Pisang\n" +
  "warna: Kuning\n" +
  "ada bijinya: tidak\n" +
  "harga: 5000";

let lines = data.split('\n');

lines.forEach((line)=>{
  console.log(line);
});

    var objArr = new Array();  

    for(let li = 0; li < lines.length; li=li+4){

      let line1 = lines[li+0].split(':');
      let line2 = lines[li+1].split(':');
      let line3 = lines[li+2].split(':');
      let line4 = lines[li+3].split(':');

      let obj = new Object();
      obj['nama'] = line1[1].toString().trim(); 
      obj['warna'] = line2[1].toString().trim();
      obj['ada bijinya'] = line3[1].toString().trim();
      obj['harga'] = line4[1].toString().trim();

      objArr.push(obj);

    }

console.log(objArr);

jwb3.innerHTML = JSON.stringify(objArr[0]);