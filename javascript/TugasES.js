let jawaban1 = document.getElementById('jawabanNo1');
let jawaban2 = document.getElementById('jawabanNo2');
let jawaban3 = document.getElementById('jawabanNo3');
let jawaban4 = document.getElementById('jawabanNo4');
let jawaban5 = document.getElementById('jawabanNo5');

// Soal no.1
/* const golden = function goldenFunction(){
  console.log("this is golden!!")
} */

// jawaban

const golden = ()=>{
	console.log("this is golden!!")
	return "this is golden!!"
}

jawaban1.innerHTML = golden()


// Soal no.2
/* const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return firstName + " " + lastName 
    }
  }
} */
 
//Driver Code 
/* newFunction("William", "Imoh").fullName() */ 

const newFunction = (firstName, lastName)=>{
	return{
		firstName: firstName,
		lastName: lastName,
		fullName: () =>{
			console.log(firstName + " " + lastName)
			return firstName + " " + lastName
		}
	}
}

jawaban2.innerHTML = newFunction("William", "Imoh").fullName()

// Soal no.3

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject

console.log(firstName, lastName, destination, occupation)

jawaban3.innerHTML = `${firstName}, ${lastName}, ${destination}, ${occupation}`

// Soal no.4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
/* const combined = west.concat(east)
//Driver Code
console.log('cara ES5')
console.log(combined) */

const combined = [...west, ...east]
console.log('cara ES6')
console.log(combined)
jawaban4.innerHTML = combined

// soal no.5
const planet = "earth" 
const view = "glass" 
/* var before = 'Lorem ' + view + 'dolor sit amet, ' +
	   'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +     
	   'incididunt ut labore et dolore magna aliqua. Ut enim' +     
	   ' ad minim veniam'   
// Driver Code 
console.log('cara ES5')
console.log(before) */ 

let after = `Lorem ${view} dolor sit amet, ` +
	   `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +     
	   'incididunt ut labore et dolore magna aliqua. Ut enim' +     
	   ' ad minim veniam'   
// Driver Code 
console.log('cara ES6')
console.log(after)
jawaban5.innerHTML = after
